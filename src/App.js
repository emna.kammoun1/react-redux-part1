import React from "react";
//importer les information
import { UseSelector, useSelector } from "react-redux/es/hooks/useSelector";

import {useDispatch} from 'react-redux'
//importer les action
import { increment } from "./actions";  
import { decrement } from "./actions";
function App() {
  // la valeur renvoyée sera la valeur de state.counter
  const counter = useSelector(state => state.counter)
  const isLogged = useSelector(state => state.isLogged)
  const dispatch = useDispatch()

  return (
    <div className="App">
      <h1>Counter {counter}</h1>
      <button onClick={() => dispatch(increment(5))}>+</button>
      <button onClick={() => dispatch(decrement())}>- </button>

      {isLogged ? <h3>Valuable information</h3> : ''}
    </div>
  );
}

export default App;
